# 范例说明

本项目仅简单介绍golang、gin、gorm构建web framework的过程

## 架构

![](gin.sample.png)

各个模块进行简单说明

- client
client 可以通过如下命令访问，可以得到 ```{"message":"Wroking"}``` 与 ```{"Name":"","core":"L1212","message":"Found it"}```

```s
curl localhost:8080/ping
curl localhost:8080/prod
```

- gin 是一个高性能的Web Framework，提供的功能挺多，可以参考official介绍，个人作为开发者，可以参考并使用其开发框架
    - 同类产品中又 echo, beego等框架

- models下定义了与数据库交换的模块， 包括：结构体（表结构定义）、数据表提供的功能（查询、新增、删除等等）

- controllers定义了数据处理的逻辑， 这里实现的是从数据库获取一条记录，然后显示转化为json格式返回到前端

- db定义了数据库访问的基本配置

- gorm定义了与数据库层交换的框架， 这样用户可以隔离出应用与数据库的区别
    - golang同类产品中又 xorm等等；

- 本次使用MySQL数据库作为数据存放

## 后端开发人员转入WEB开发的疑惑

对于长期在后端开发的人员，对当个模块及一个完整的程序开发流程比较熟悉，但是对于WEB的框架的时候，涉及到几个概念混在一块，理解起来比较难受， 包括：

- orm是什么
- web framework是什么
- mvc框架是什么
- http调用如何处理

简单对各个概念进行剥离理解

### orm
作为与数据库开发直接交互的工程师，对oracle的Proc*C, DB2的 esql\*C等，是直接访问数据库，这个过程是直接编写查询、定义游标、定义事务等等；

但是这样存在一个问题，就是应用与数据库的关联关系非常的强，如果涉及到更换数据库的时候，应用的改动影响比较大；

而ORM则为了避免应用与数据操作过程的强关联，定义了一组通用的方法来实现；

### web framework

如何理解框架，对于开发人员来说比较困难， 因为框架就像是抽象出来的概念， 比如： iso8583规范、 如小朋友理解什么是上班等等， 甚至有人说Framework就是扯淡的事情

我对应WEB Framework的理解是，人们对于混在一块的内容进行模块划分， 包括MVC

## 了解到什么

首先，所有的这些完全可以放在一个文件里面， 或者一个目录下面，但是为了更好的管理，团队协助开发需要对模块进行划分；

- 一个误区，照着项目的模板抄一次，可以知道为什么； 但是有些地方还是不懂， 可以试着从一个文件模块中进行拆分；
- 逐个模块验证， 如果验证数据库就是验证数据库， 验证route就是验证route；


## 参考
多谢如下文章让我开始上手写web

- 如何实现model、controller, [go-gin-boilerplate](https://github.com/vsouza/go-gin-boilerplate)
- 参考使用如何加载配置文件, [git-go-websiteskeleton ](https://github.com/jadekler/git-go-websiteskeleton.git)
- 参考生产html模板example [Go Web Examples ](https://gowebexamples.com/templates/) 

