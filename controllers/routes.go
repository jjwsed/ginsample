package controllers

import (
	"github.com/gin-gonic/gin"
)

func RegisterRoutes(r *gin.Engine) {

	new(ProductController).RegisterRoute(r) //注册产品列表

	new(PING).RegisterRoute(r) // 注册ping
}
