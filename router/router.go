package router

import (
	"ginsample/controllers"

	"github.com/gin-gonic/gin"
)

func Init() {

	r := Router() //

	r.LoadHTMLGlob("templates/*") //加载所有templates的配置文件

	r.Run() //启动8080端口
}

func Router() *gin.Engine {

	r := gin.New()
	r.Use(gin.Logger())
	r.Use(gin.Recovery())

	controllers.RegisterRoutes(r)

	return r
}
