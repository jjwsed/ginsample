package db

import (
	"fmt"

	"github.com/Unknwon/goconfig"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jinzhu/gorm"
)

const ConfigPath = "config/env.ini"

var db *gorm.DB
var config *goconfig.ConfigFile
var url string

func init() {
	var err error

	config, err = goconfig.LoadConfigFile(ConfigPath) // 获取配置文件,但是感觉每次都需要读取一次
	if err != nil {
		panic("config is not exist")
	}

	conf, err := config.GetSection("mysql") // 获取数据库配置
	if err != nil {
		panic("get mysql config error:")
	}

	url = fmt.Sprintf("%s:%s@tcp(%s:%s)/%s?charset=%s&parseTime=True&loc=Local",
		conf["user"],
		conf["pass"],
		conf["host"],
		conf["port"],
		conf["dbname"],
		conf["charset"])

	db, err = gorm.Open("mysql", url)
	if err != nil {
		panic("failed to connect database")
	}
	db.LogMode(true)

	// Migrate the schema
	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)

	//db.Close()  不知道什么时候关闭
}

func NewOne() *gorm.DB {

	// db, err := gorm.Open("mysql", url)
	// if err != nil {
	// 	panic("failed to connect database")
	// }
	return db
}

func Close(db *gorm.DB) {
	db.Close()
}
